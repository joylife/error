(function () {
    "use strict";

    var assert = require("chai").assert;
    var error = require("./");
    var isWithJoyError = error.isWithJoyError;

    describe("error", function () {
        describe("server", function () {
            var classInfos = [{
                className: "BadGateway",
                message: "Bad Gateway"
            }, {
                className: "InternalServerError",
                message: "Internal Server Error"
            }, {
                className: "NotImplemented",
                message: "Not Implemented"
            }];

            classInfos.forEach(function (classInfo) {
                it("exports error class server." + classInfo.className, function (callback) {
                    var Class = error.server[classInfo.className];
                    assert(Class);
                    var err = new Class("yo");
                    assert(err instanceof Error);
                    assert(isWithJoyError(err));
                    assert(err.statusCode);
                    assert(err.name);
                    assert.strictEqual(err.message, "yo");
                    var stack = err.stack;
                    assert.equal(typeof stack, 'string');
                    assert(/    at new NewErrorClass.+error\/error.js/.test(stack));
                    assert(/    at Runner.runTest.+node_modules\/mocha\/lib\/runner.js/.test(stack));
                    return setImmediate(callback);
                });
            });
        });

        describe("client", function () {
            var classInfos = [{
                className: "BadRequest",
                message: "Bad Request"
            }, {
                className: "Unauthorized",
                message: "Unauthorized"
            }, {
                className: "Forbidden",
                message: "Forbidden"
            }, {
                className: "NotFound",
                message: "Not Found"
            }];

            classInfos.forEach(function (classInfo) {
                it("exports error class client." + classInfo.className, function (callback) {
                    var Class = error.client[classInfo.className];
                    assert(Class);
                    var err = new Class("yo");
                    assert(err instanceof Error);
                    assert(isWithJoyError(err));
                    assert(err.statusCode);
                    assert(err.name);
                    assert.strictEqual(err.message, "yo");
                    var stack = err.stack;
                    assert.equal(typeof stack, 'string');
                    assert(/    at new NewErrorClass.+error\/error.js/.test(stack));
                    assert(/    at Runner.runTest.+node_modules\/mocha\/lib\/runner.js/.test(stack));
                    return setImmediate(callback);
                });
            });
        });

        describe("isWithJoyError", () => {
            it("returns false for generic Errors", () => {
                assert.equal(isWithJoyError(new Error()), false);
            });

            it("returns true for Joy Errors", () => {
                var InternalServerError = error.server.InternalServerError;
                assert.equal(isWithJoyError(new InternalServerError()), true);

                var NotFound = error.client.NotFound;
                assert.equal(isWithJoyError(new NotFound()), true);
            });
        });
    });
})();
