(function () {
    // Define Http Error Classes
    // http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html

    "use strict";

    var util = require("util");

    exports.Error = function (statusCode, name, message) {
        var This = this;

        var tmp = Error.call(this, message);
        This.name = tmp.name = name;
        This.message = tmp.message;
        This.statusCode = statusCode;

        if (Error.captureStackTrace) {
            // Best Practices RE: stacks in custom Error Classes
            //   https://www.bennadel.com/blog/2828-creating-custom-error-objects-in-node-js-with-error-capturestacktrace.htm
            //   https://github.com/joyent/node-verror
            Error.captureStackTrace(This, exports.Error);
        }
        else {
            // FIXME:  this is highly unconventional; a String is expected
            //   for example, this implementation breaks `jest` Test Suites
            //   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error/stack
            //   "Each step will be separated by a newline, ..."
            This.stack = (tmp.stack || "").split("\n");
        }
    };
    util.inherits(exports.Error, Error);

    exports.Error.prototype.isInformational = function () {
        var This = this;
        return This.statusCode >= 100 && This.statusCode < 200;
    };

    exports.Error.prototype.isSuccessfull = function () {
        var This = this;
        return This.statusCode >= 200 && This.statusCode < 300;
    };

    exports.Error.prototype.isRedirection = function () {
        var This = this;
        return This.statusCode >= 300 && This.statusCode < 400;
    };

    exports.Error.prototype.isClient = function () {
        var This = this;
        return This.statusCode >= 400 && This.statusCode < 500;
    };

    exports.Error.prototype.isServer = function () {
        var This = this;
        return This.statusCode >= 500;
    };


    exports.isWithJoyError = function (err) {
        // duck-typing is the way to go, vs. `instanceof`
        //   two independently `require()d` modules / packages are not `instanceof` each other,
        //   even if the source code is *identical*.
        //   we'll sniff for our mis-spelled method :)
        return (err.isSuccessfull instanceof Function);
    }


    exports.server = {};
    exports.client = {};

    var createErrorClass = exports.createErrorClass = function (statusCode, name) {

        var NewErrorClass = function (message) {
            NewErrorClass.super_.call(this, statusCode, name, message);
        };

        util.inherits(NewErrorClass, exports.Error);

        return NewErrorClass;
    };

    exports.client.BadRequest = createErrorClass(400, "Bad Request");
    exports.client.Unauthorized = createErrorClass(401, "Unauthorized");
    exports.client.Forbidden = createErrorClass(403, "Forbidden");
    exports.client.NotFound = createErrorClass(404, "Not Found");
    exports.client.Conflict = createErrorClass(409, "Conflict");
    exports.client.Gone = createErrorClass(410, "Gone");
    exports.client.TooManyRequests = createErrorClass(429, "TooManyRequests");
    exports.client.ApplicationException = createErrorClass(520, "Application Exception");
    exports.server.InternalServerError = createErrorClass(500, "Internal Server Error");
    exports.server.NotImplemented = createErrorClass(501, "Not Implemented");
    exports.server.BadGateway = createErrorClass(502, "Bad Gateway");
    exports.server.ApplicationException = createErrorClass(520, "Application Exception");
})();
